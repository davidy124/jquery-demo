function deleteCityById(id) {
    console.log(id)
}

function appendCity(city) {
    let citiesElement = $('#city_table');
    if (citiesElement) {
        let tableContent = "<tr><td style='width:70%'>"+city+"</td><th scope='row' style='width:30%;'><button id='"+city+"' type='button' class='btn btn-secondary' onclick='deleteCityById(this.id)'>Delete</button></th></tr>";
        citiesElement.append(tableContent);
    }
}

$(function () {

    const cities = ["Mumbai", "Bengaluru", "New York", "Rome", "Seattle",
        "Paris", "Dubai", "Singapore", "Perth", "New Zealand"];

    function initCities() {
        let citiesElement = $('#city_table');
        citiesElement.empty();
        $.each(cities, function (_, city) {
            appendCity(city);
        });
    }

    $("#add_city_btn").on("click", function () {
        let newCity = $("#new_city").val();
        if (newCity) {
            appendCity(newCity)
        }
    });

    initCities();
});